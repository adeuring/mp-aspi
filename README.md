# Micropython performance improvements with nonblocking SPI access on an ESP32

## Motivation

While tinkering with [Peter Hinch's micropython-micro-gui library](https://github.com/peterhinch/micropython-micro-gui),
I noticed that updating an LCD display can take a relatively long time.
Peter mentions this too in the
[documentation of Micro-GUI](https://github.com/peterhinch/micropython-micro-gui/blob/main/README.md#18-performance-and-hardware-notes).

I am using a 360x240 pixel display with an ILI9341 driver chip, connected via
SPI to an ESP32 running a recent self-compiled version of Micropython, commit
cfd3b70934791abc07f9476a956781de92ddf715.

16 bits must be sent to the ILI9341 chip for each pixel; the SPI
clock frequency is set to 80/3 MHz (the source code in this repo specifies
30_000_000 as the frequency but that value is converted by the SPI driver
to the next smaller possible value: the ESP32 generates the SPI clock by
dividing an internal 80MHz clock). The minimum time required to refresh the
entire display is thus 16 * 360 * 240 * 3 / 80_000_000 seconds, circa 52ms.
The transfer of one 240 pixel line needs
16 * 240 * 3 / 80_000_000 seconds, or 144µs.

Measurements with a DSO showed that the ILI9486 driver from the Micro-GUI
library needs ca 100ms or more to update the display, i.e., roughly twice
as long as the time needed to send the display data over the SPI bus.

THe reason for this slowdown is simple: The ILI9486 driver module uses
internally a frame buffer with 4 bits per pixel to save RAM.
These 4-bit values must be mapped to 16-bit values as needed by the driver
chip; this conversion takes ca 90µs for 240 pixels.

The class `machine.SPI` uses blocking methods to transfer data via the SPI bus,
hence the ILI9486 driver can either map pixel data from 4 to 16 bits, or
it can send the 16-bit data to the display.
Since the ESP32 uses DMA for its hardware SPI interfaces, the processor
just waits until all data is transferred before a method like `SPI.write()`
return. Instead it could do more useful stuff: do the 4->16 bit mapping for
the next line while a line is being tansferred.

To see what is possible, I tinkered  with the file
`ports/esp32/machine_hw_spi.c`:
Added a copy of it to this repo under the name modaspi.c, added the
"C bureaucracy" to turn it into a C extension for Micropython and changed
it so that data transfer methods of the new `class _ASPI` do not (always) block.

## Timing measurements

### Physical setup

I used a Rigol DS1054Z to measure the timing of the SPI data transfer to
the display. Channel 1 was connected to the SPI clock signal; channel 3
was connected to the CS signal of the display driver chip. Channel 2
was connected to a GPIO pin that is pulled low while the driver is mapping
4-bit frame buffer pixel values to 16 bit values to be sent to the display
(global variable `pin_lcopy` in `ili9486.py` in this repo). Channel 4 was
connected to a GPIO pin that is pulled low while one of the methods
`ILI9486.do_refresh_machine_spi()` or `ILI9486.do_refresh_aspi_synchronous()`
are running (global variable `pin_fb_update` in `ili9486.py`). These methods
update the LCD display with data from the frame buffer.

### Software setup

I ran the example application [`gui.demos.aclock`](https://github.com/peterhinch/micropython-micro-gui/blob/main/gui/demos/aclock.py) from the micro-gui repo
as a simple way to get continuous activity on the SPI bus.

The files `ili9486.py` and `hardware_setup.py` in this repo obviously differ
from their
original versions in https://github.com/peterhinch/micropython-micro-gui ;
The source code of the other Micro-GUI files was used unmodified
(commit 43c19b7b4866b01398465d52c86c720837bb269f).

#### Modifications in `ili0686.py`

The functions `_lcopy()` and `_lscopy()`, which map 4-bit pixel values
from frame buffer values to 16-bit values for the LCD display, toggle the
GPIO pin `pin_lcopy`.

The original method `ILI9486.do_refresh()` is renamed to
`ILI9486.do_refresh_machine_spi()`. I added a second variant of that method,
called `do_refresh_aspi_synchronous()`, that is intended to be used with
the class `_ASPI` from the new C module. `ILI9486.__init__()` decides
from the type of the call parameter `spi` which of the two variants of
`do_refresh` will be used.

I added the decorator `@micropython.native` to `do_refresh_machine_spi()`
and `do_refresh_aspi_synchronous()`. ([A comment by Peter Hinch](https://github.com/peterhinch/micropython-micro-gui/blob/fc7c91fa204385a1049be5b0cfde1dbcd0b248b8/drivers/ili94xx/ili9486.py#L136) suggests that using this
decorator makes "almost no difference". I disagree; IMHO the speed improvement
is not useless, although not that big.)

The line `await asyncio.sleep_ms(0)` in these methods was disabled for six
of the eight measurements shown below because they add noticable jitter.
These calls are of course reasonable in a real application but the jitter
is a bit irritating in a measurement.

[Diff of all modifications to ili9486.py](https://gitlab.com/adeuring/mp-aspi/-/commit/d2e4fe2caf3ac148ef1ef416bd49775eb0419d84#cc1e2d895afcfde48429b805c65153e4c7322873).


### Results

A screenshot from the DSO that shows the timing of the transfer of the data
for a single 240 pixel line to the display when machine.SPI is used:

![Line transfer timing of class ILI9486, using machine.SPI](dso-screenshots/timing-lcopy-spi-transfer-machine-spi.png)*Line transfer timing of class ILI9486, using machine.SPI*

Channel 1 (yellow) shows the SPI clock, channel 2 (cyan) shows the GPIO pin
that is toggled in `_lcopy()` and `_lscopy()` - variable `pin_lcopy` in file
`ili9486.py` - , channel 3 (magenta) is connected
to the CS line of the ILI9341 chip; channel 4 (blue) is connected to the GPIO
pin which is toggled in `ILI9486.do_refresh_machine_spi()` and
`ILI9486.do_refresh_aspi_synchronous()` - variable `pin_fb_update` in
`ili9486.py`.

So, `_lcopy()` or `_lscopy()` needs ca 90µs to execute; the SPI bus is
active for 240 * 16 * 3 / 80 µs -> 144µs. Zhe total time needed to process
one line (prepare the data for one line, send it to the display and
"loop overhead" to iterate over the frame buffer lines) is ca 290µs, or
twice as long as the plain SPI data transfer.

If class `_aspi._ASPI` is used instead, I get this timing:

![Line transfer timing of class ILI9486, using _aspi._ASPI](dso-screenshots/timing-lcopy-spi-transfer-aspi.png)*Line transfer timing of class ILI9486, using _aspi._ASPI*

Now the time to transfer data for 240 pixels is between 160µs and 174µs.

More interesting is the time needed to transfer the data for the entire display.

![Full image transfer timing of class ILI9486, using machine.SPI](dso-screenshots/timing-full-display-update-machine-spi.png)*Full image transfer timing of class ILI9486, using machine.SPI*

Channel 4 (connected to GPIO `pin_fb_update`) shows that the display update
takes 93.6ms to 94.0ms.

When `_aspi._ASPI` is used instead of `machine.SPI`, the update is, as expected,
faster:

![Full image transfer timing of class ILI9486, using _aspi._ASPI.](dso-screenshots/timing-full-display-update-aspi.png)*Full image transfer timing of class ILI9486, using _aspi._ASPI. (Note
the right part of the image: Channel 4 is high for more than 40ms but
that time is not included in the period statistics. ISTM that the
Rigol DS1054Z includes only the leftmost period on the DSO display in the
statistics)*

Using class `_ASPI`, the display update needs only 52.8ms to 53.2ms, or
ca 57% of the time needed with `machine.SPI`.

The decorator `@micropython.native` makes a small but noticable difference:

![Full image transfer timing of class ILI9486, using machine.SPI but without the decorator `@micropython.native` for `ILI.9486.do_refresh_machine_spi()`](dso-screenshots/timing-full-display-update-machine-spi.png)*Full image transfer timing of class ILI9486, using machine.SPI but without the decorator `@micropython.native` for `ILI.9486.do_refresh_machine_spi()`.*

When it is disabled, the time to update the display using `machine.SPI`
increases to 98.0ms .. 98.4ms, 4.4ms more than with the decorator. The
additional time for removing `@micropython.native` from
`ILI94.do_refresh_aspi_synchronous()` is a bit larger. Without
the decorator, the display update needs 59.2ms .. 60.0ms, a difference of
6.4ms .. 6.8ms:

![Full image transfer timing of class ILI9486, using `_aspi._ASPI` but without the decorator `@micropython.native` for `ILI.9486.do_refresh_aspi_synchronous()`](dso-screenshots/timing-full-display-update-aspi-no-native-decorator.png)*Full image transfer timing of class ILI9486, using `_aspi._ASPI` but without the decorator `@micropython.native` for `ILI.9486.do_refresh_aspi_synchronous()`.*

As mentioned above, the lines

```await asyncio.sleep_ms(0)```

in `ILI94.do_refresh_aspi_synchronous()` and `ILI.9486.do_refresh_machine_spi()`
were disabled for the timing measurements shown above to get more stable
values. When these lines are enabled (more specifically, when the source
code is the same as in commit
d2e4fe2caf3ac148ef1ef416bd49775eb0419d84 of this repo), the display
update takes 101ms .. 116ms with machine.SPI and 58ms .. 60ms with _aspi._ASPI.

![Full image transfer timing of class ILI9486, using machine.SPI, source code as in commit d2e4fe2caf3ac148ef1ef416bd49775eb0419d84](dso-screenshots/timing-full-display-update-machine-spi-with-sleep-call.png)*Full image transfer timing of class ILI9486, using machine.SPI, source code as in commit d2e4fe2caf3ac148ef1ef416bd49775eb0419d84.*

![Full image transfer timing of class ILI9486, using _aspi._ASPI, source code as in commit d2e4fe2caf3ac148ef1ef416bd49775eb0419d84](dso-screenshots/timing-full-display-update-aspi-with-sleep-call.png)*Full image transfer timing of class ILI9486, using _aspi._ASPI, source code as in commit d2e4fe2caf3ac148ef1ef416bd49775eb0419d84.*

## Remarks on the class `_aspi._ASPI`

This class is in its current state barely good enough to be used at least
in an LCD driver like Peter Hinch's `ili9486.py` module but far away from
being generally useful. The main idea is for now nothing more than to show
that using nonblocking access to the SPI bus can be useful even when the
blocking time is less than one millisecond.

Currently implemented API:

The general idea is that a configurable number of "transfers" (by default two)
can be queued. A transfer is queued by calling
`_aspi._ASPI.start_transfer(write_buffer, read_buffer)`. One of the parameters
`write_buffer`, `read_buffer` may be None, but not both. The data of several
queued transfers is sent over the SPI bus in the order in which they are
queued. If `_aspi._ASPI.start_transfer(write_buffer, read_buffer)` is
called while all allocated transfers (constructor parameter `max_transfers`)
are "active", the method blocks until one already queued transfer is finished.

`_aspi._ASPI.start_transfer()` returns a `transfer_id`, an integer that, well,
identifies the transfer. The `transfer_id` can be used to poll the status
of the transfer by calling `_aspi._ASPI.transfer_finished(transfer_id)`.
This method return `False` if the transfer is still queued or if its data is
currently sent via the SPI bus, otherwise it returns `True`.

`_aspi._ASPI.finish_transfer(transfer_id)` is a blocking call that waits
until the transfer is finished.

_aspi._ASPI.can_start_transfer() returns True if the transfer queue is not full,
i.e., if at least one transfer can be immiediately queued with a
`_aspi._ASPI.start_transfer()` call without blocking.

_aspi._ASPI.write(buffer, wait_until_finished=True) a C implementation
equivalent to this Python code:

```
def _ASPI(self, buffer, wait_until_finished=True):
    transfer_id = self.start_transfer(buffer, None)
    if wait_until_finished:
        self.finish_transfer(transfer_id)
    return transfer_id
```

Constructor: `_aspi._ASPI(baudrate=1000000, *, polarity=0, phase=0, bits=8, firstbit=SPI.MSB, sck=None, mosi=None, miso=None, max_transfer_length=-1, max_transfers=-1)`

The parameters `baudrate`, `polarity`, `phase`, `bits`, `firstbit`, `sck`,
`mosi`, `miso` are identical to those of [`class SPI`](https://docs.micropython.org/en/latest/library/machine.SPI.html#class-spi-a-serial-peripheral-interface-bus-protocol-controller-side).
`max_transfer_length` determines the maximum number of bytes that can
transferred with one call of `_aspi._ASPI.start_transfer()`  and
`_aspi._ASPI.write()`. If not specified, the max transfer length is 4092.

`max_transfers` specifies the number of SPI transfers that can be "active"
at the same time, i.e, the length of the internal transfer queue.

### Limitations

- If specified, the constructor parameter `bits` must be 8. Other word lengths
  for the SPI bus are not supported.
- Attempts to concurrently use machine.SPI and _aspi._ASÜI for the same
  bus lead to an utter mess.
- Many developers, myself included, consider untested code to be broken
  by definition. In this sense the `_aspi` module is completely broken as no
  tests exist at all. I can live with this – the intended purpose of this
  repo is  currently only to show that nonblocking access to the SPI bus
  can be useful for some Micropython applications.

## Installation

- Clone the Micropython source code from https://github.com/micropython/micropython
- Copy the file `modaspi.c` from this repo to the directory `ports/esp32` in
  the Micropython source tree. Or clone this repo and add a symlink
  to `ports/esp32` that points to the file `modaspi.c` in your copy of this
  repo.
- Add `modaspi.c` to `MICROPY_SOURCE_PORT` in the file
  `ports/esp32/main/CMakeLists.txt`.

  Diff:

  ```
  --- a/ports/esp32/main/CMakeLists.txt
  +++ b/ports/esp32/main/CMakeLists.txt
  @@ -85,6 +85,7 @@ set(MICROPY_SOURCE_PORT
       ${PROJECT_DIR}/mpthreadport.c
       ${PROJECT_DIR}/machine_rtc.c
       ${PROJECT_DIR}/machine_sdcard.c
  +    ${PROJECT_DIR}/modaspi.c
  ```
- Copy the directory `gui` from
  https://github.com/peterhinch/micropython-micro-gui to the directory
  `ports/esp32/modules` in the Micropython source tree. Better, clone the
  micro-gui repo and add a symlink to `ports/esp32/modules` that points
  to the `gui` directory in your local copy of the micro-gui repo.
- Compile and flash the Micropython firmware as described in
  https://github.com/micropython/micropython/tree/master/ports/esp32#readme
- Connect a display module supported by the ILI9486 driver to the ESP32
- Connect a few pushbuttons, perhaps also a rotary encoder, to the ESP32,
  as described in the [micropython-micro-gui documentation](https://github.com/peterhinch/micropython-micro-gui/blob/main/README.md#14-navigation).
  Pushbuttons are not strictly necessary but many demos from the micro-gui
  repo are a bit boring without them.
- Edit the file `hardware_setup.py` from this repo to match your wiring
  of the display and of the pushbuttons.
- Upload the Python files `hardware_setup.py`, `ili9486.py`, `run_demo.py`
  from this repo to the ESP32.
- Upload the file `drivers/boolpalette.py` from the micropython-micro-gui repo
  to the ESP32's root directory.


## Trying `_aspi._ASPI`

Connect to the REPL of the ESP32 and type `import run_demo`. This module
lists [all example applications of micro-gui](https://github.com/peterhinch/micropython-micro-gui/tree/main/gui/demos) and allows to choose one. Next,
the module asks if machine.SPI or _aspi._ASPI should be used for the selected
demo. Finally it asks if the display should be configured for landscape or
portrait orientation.


### Beware

1. Some demos do not work on the ESP32 - I did not bother
to remove those from the list.
2. Some demos work only in landscape orientation.


## Using asyncio?

Asynchronous I/O got in (Micro-)Python a lot of traction in the last years,
for good reasons. Indeed I intended at first to use an
SPI module that provides coroutines for the SPI data transfer:

```
from _aspi import _ASPI
from select import POLLIN, POLLOUT
import uasyncio.core as async_core
from uio import IOBase


class _IOCtlReadHelper(IOBase):
    def __init__(self, aspi, tfid):
        self._aspi = aspi
        self._tfid = tfid

    def ioctl(self, req, flags):
        if req == 3:
            if flags & POLLIN and self._aspi.transfer_finished(self._tfid):
                return POLLIN
            return 0
        return None


class _IOCtlWriteHelper(IOBase):
    def __init__(self, aspi):
        self._aspi = aspi

    def ioctl(self, req, flags):
        if req == 3:
            if flags & POLLOUT and self._aspi.can_start_transfer():
                return POLLOUT
            return 0
        return None


class ASPI(_ASPI):
    def __init__(
            self, id, baudrate=500_000, polarity=0, phase=0, bits=8,
            firstbit=SPI.MSB, sck=None, mosi=None, miso=None,
            max_transfer_length=-1, max_transfers=2):
        super().__init__(
            id, baudrate, polarity, phase, bits, firstbit,
            sck, mosi, miso, max_transfer_length, max_transfers)
        self._wr_ioctl = _IOCtlWriteHelper(self)
        self._rd_ioctl = tuple(
            _IOCtlReadHelper(self, tfid) for tfid in range(max_transfers))

    async def awrite(self, data):
        if not self.can_start_transfer():
            yield async_core._io_queue.queue_write(self._wr_ioctl)
        tf_id = self.start_transfer(data, None)
        yield async_core._io_queue.queue_read(self._rd_ioctl[tf_id])
```

`class ASPI` can be used in class `ILI9486` in `ili9486.py` like so:

```
spi = ASPI(...)
linebufs = [bytearray(...), bytearray(...)]
tasks = [None, None]
bufindex = 0

for line in number_of_display_lines:
    if tasks[bufindex] is not None:
        await tasks[bufindex]
    _lcopy(linebufs[bufindex], ...)
    tasks[bufindex] = uasyncio.create_task(spi.awrite(linebufs[bufindex]))
    bufindex = 1 - bufindex
await uasyncio.gather(*tasks)
```

This turned out to be a quite naive idea: The transfer of the data for all
lines takes about 0.5 seconds. Each task that runs `ASPI.awrite()` uses the
SPI bus for 144 µs; its "plain" runtime, task switches excluded, is
probably less than 200µs. Hence the overhead added by the task creation itself,
by the task switches that can (and must) happen within the coroutine and
other saynnc related overhead seem to need much more time than the "pure"
runtime of the coroutine `awrite()` itself.

If the SPI clock is slow enough and the amount of data to transfer is big
enough that the entire transfer needs at least a few milliseconds, the
approach of `class ASPI` might begin to make sense. I am not aware though
of any real-world use case that would match these conditions.
