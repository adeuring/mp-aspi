def get_int_val(prompt, minval, maxval, default):
    result = None
    while result is None:
        result = input(prompt)
        if result == '':
            result = default
        else:
            try:
                result = int(result)
            except ValueError:
                print('Type a number')
                result = None
                continue
            if result < minval or result >= maxval:
                print(f'Number must be between 1 and {len(demos)-1}')
                result = None
    return result

demos = [
    "aclock", "active", "adjuster", "adjust_vec", "audio", "bitmap",
    "calendar", "checkbox", "date", "dialog", "dropdown", "linked_sliders",
    "listbox", "menu", "plot", "primitives", "qrcode", "screen_change",
    "screens", "simple", "slider_label", "slider", "tbox", "tstat",
    "various", "vtest",
    ]

print('Available demos:')
for index, name in enumerate(demos):
    print(index, name)
dno = get_int_val(
    'demo module index (default: 0, gui.demos.aclock): ', 0, len(demos), 0)
spi_variant = get_int_val(
    'SPI implementation (0: machine.SPI, 1: sync ASPI (default: 1): ', 0, 2, 1)
orientation = get_int_val(
    'Display orientation (0: landscape, 1: portrait, default: 0)', 0, 2, 0)
__import__('gui.demos.' + demos[dno])
