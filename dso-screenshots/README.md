# DSO screenshots with SPI output timings

All screenshots were made by running Peter Hinch's Micro-GUI demo `aclock.py`.

Oscilloscope inputs:

|DSO Channel   |Signal                                                  |
|--------------|--------------------------------------------------------|
|CH1 (yellow)  |SPI clock                                               |
|CH2 (cyan)    |CS                                                      |
|CH3 (magenta) |Pin "lcopy", low while one of the functions _lcopy() or _lscopy() from ili9486.py is running. |
|CH4 (blue)    |Pin "fb update", low while one of the methods ILI9486.do_refresh_machine_spi() or ILI9486.do_refresh_aspi_synchronous() is running. |

Screenshots
- `timing-full-display-update-aspi.png`
- `timing-full-display-update-machine-spi.png`
- `timing-lcopy-spi-transfer-aspi.png`
- `timing-lcopy-spi-transfer-machine-spi.png`

made with the lines `await asyncio.sleep_ms(0)` in
`ILI9486.do_refresh_machine_spi()` and `ILI9486.do_refresh_aspi_synchronous()`
commented out in order to get more stable statistics for the time span
while CH4 is low.

Patches applied on commit cfd3b70934791abc07f9476a956781de92ddf715
for these measurements:

```
--- a/ili9486.py
+++ b/ili9486.py
@@ -220,7 +220,7 @@ class ILI9486(framebuf.FrameBuffer):
                         self._spi.write(lb)
                     line += lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             else:  # Landscape: write sets of cols. lines is no. of cols per segment.
                 cargs = (self.height << 9) + (self.width << 18)  # Viper 4-arg limit
                 sc = self.width - 1  # Start and end columns
@@ -235,7 +235,7 @@ class ILI9486(framebuf.FrameBuffer):
                     sc -= lines
                     ec -= lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             pin_fb_update(1)
 
     def show_aspi_synchronous(self):  # Physical display is in portrait mode
@@ -311,7 +311,7 @@ class ILI9486(framebuf.FrameBuffer):
                     self._spi.finish_transfer(transfers[1-lbi])
                     line += lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             else:  # Landscape: write sets of cols. lines is no. of cols per segment.
                 cargs = (self.height << 9) + (self.width << 18)  # Viper 4-arg limit
                 sc = self.width - 1  # Start and end columns
@@ -334,5 +334,5 @@ class ILI9486(framebuf.FrameBuffer):
                     sc -= lines
                     ec -= lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             pin_fb_update(1)
```

Screenshots
- `timing-full-display-update-aspi-no-native-decorator.png`
- `timing-full-display-update-machine-spi-no-native-decorator.png`

Without the `@micropython.native` decorator for
`do_refresh_machine_spi()` and `do_refresh_aspi_synchronous()`.

Patches applied on commit cfd3b70934791abc07f9476a956781de92ddf715
for these measurements:

```
a/ili9486.py b/ili9486.py
index b217b14..233ee43 100644
--- a/ili9486.py
+++ b/ili9486.py
@@ -196,7 +196,7 @@ class ILI9486(framebuf.FrameBuffer):
                 self._spi.write(lb)
         self._cs(1)
 
-    @micropython.native
+    #@micropython.native
     async def do_refresh_machine_spi(self, split=4):
         async with self._lock:
             pin_fb_update(0)
@@ -220,7 +220,7 @@ class ILI9486(framebuf.FrameBuffer):
                         self._spi.write(lb)
                     line += lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             else:  # Landscape: write sets of cols. lines is no. of cols per segment.
                 cargs = (self.height << 9) + (self.width << 18)  # Viper 4-arg limit
                 sc = self.width - 1  # Start and end columns
@@ -235,7 +235,7 @@ class ILI9486(framebuf.FrameBuffer):
                     sc -= lines
                     ec -= lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             pin_fb_update(1)
 
     def show_aspi_synchronous(self):  # Physical display is in portrait mode
@@ -278,7 +278,7 @@ class ILI9486(framebuf.FrameBuffer):
         self._cs(1)
         pin_fb_update(1)
 
-    @micropython.native
+    #@micropython.native
     async def do_refresh_aspi_synchronous(self, split=4):
         async with self._lock:
             pin_fb_update(0)
@@ -311,7 +311,7 @@ class ILI9486(framebuf.FrameBuffer):
                     self._spi.finish_transfer(transfers[1-lbi])
                     line += lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             else:  # Landscape: write sets of cols. lines is no. of cols per segment.
                 cargs = (self.height << 9) + (self.width << 18)  # Viper 4-arg limit
                 sc = self.width - 1  # Start and end columns
@@ -334,5 +334,5 @@ class ILI9486(framebuf.FrameBuffer):
                     sc -= lines
                     ec -= lines
                     self._cs(1)  # Allow other tasks to use bus
-                    await asyncio.sleep_ms(0)
+                    #await asyncio.sleep_ms(0)
             pin_fb_update(1)
```

Screenshots
- `timing-full-display-update-aspi-with-sleep-call.png`
- `timing-full-display-update-machine-spi-with-sleep-call.png`

asyncio.sleep_ms() call in do_refresh_machine_spi() and
do_refresh_aspi_synchronous() enabled.

Screenshots made with the sources from commit
cfd3b70934791abc07f9476a956781de92ddf715 without any modifications.
