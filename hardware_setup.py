# Hardware setup for an ESP32, connected to an ILI9341 display module.

# Released under the MIT License (MIT). See LICENSE.
# Copyright (c) 2021 Peter Hinch
# Modifications by Abel Deuring 2023:
# - optionally use a replacement of class machine.SPI
# - GPIO configuration for ESP32 microcontrollers, a 320x240 pixel
#   display controlled by the ILI9496 driver.

# WIRING
# ESP32     Display
# GPIO
# GPIO14    CLK  Hardware SPI0
# GPIO13    DATA (AKA SI MOSI)
# GPIO21    DC
# GPIO22    Rst
# Gnd       Gnd
# GPIO25    CS
# GPIO27    LED

# Pushbuttons and rotary encoder are wired between the pin and Gnd
# ESP pin  Meaning
# GPIO18    Operate current control
# GPIO5     Decrease value of current control
# GPIO0     Select previous control
# GPIO26    Select next control
# GPIO4     Increase value of current control

from machine import Pin, freq
import gc
from ili9486 import ILI9486 as SSD

# USE_ASPI == 0:
#    Use machine.SPI and a slightly modified variant of Peter Hinch's
#    ILI9486 driver
# USE_ASPI == 1:
#    Use aspi.ASPI and ili9486_alt.ILI9486
import run_demo
USE_ASPI = run_demo.spi_variant

if USE_ASPI:
    from _aspi import _ASPI as SPI
else:
    from machine import SPI

freq(240_000_000)
prst = Pin(22, Pin.OUT, value=1)
pcs = Pin(25, Pin.OUT, value=1)
pdc = Pin(21, Pin.OUT, value=0)  # Arbitrary pins

pled = Pin(27, Pin.OUT)
pled.value(1)

if USE_ASPI == 0:
    spi = SPI(1, sck=Pin(14), mosi=Pin(13), miso=Pin(12), baudrate=30_000_000)
else:
    spi = SPI(1, sck=Pin(14), mosi=Pin(13), miso=Pin(12), baudrate=30_000_000)
gc.collect()  # Precaution before instantiating framebuf

if run_demo.orientation == 0:
    ssd = SSD(spi, pcs, pdc, prst, usd=False, height=240, width=320, mirror=True)
else:
    ssd = SSD(spi, pcs, pdc, prst, usd=False, height=320, width=240, mirror=True)
gc.collect()
from gui.core.ugui import Display, quiet
# quiet()
# Create and export a Display instance
# Define control buttons

prev = Pin(0, Pin.IN, Pin.PULL_UP)  # Move to previous control
nxt = Pin(2, Pin.IN, Pin.PULL_UP)  # Move to next control
# The "increase" and "decrease" pins may be connected to a rotary encoder.
increase = Pin(4, Pin.IN)
decrease = Pin(5, Pin.IN)
sel = Pin(15, Pin.IN)
# The last parameter should be 0 when only pushbuttons are connected.
# A positive value indicates a rotary encoder.
display = Display(
    ssd, nxt, sel, prev, increase, decrease, 4)
