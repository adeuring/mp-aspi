/*
 * This file is based on ports/esp32/machine_hw_spi.c from the
 * MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 "Eric Poulsen" <eric@zyxod.com>
 * Copyright (c) 2023 "Abel Deuring" <adeuring@gmx.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "py/runtime.h"
#include "py/stream.h"
#include "py/mphal.h"

#include "esp_log.h"
#include "driver/spi_master.h"

// SPI mappings by device, naming used by IDF old/new
// upython   | ESP32     | ESP32S2   | ESP32S3 | ESP32C3
// ----------+-----------+-----------+---------+---------
// SPI(id=1) | HSPI/SPI2 | FSPI/SPI2 | SPI2    | SPI2
// SPI(id=2) | VSPI/SPI3 | HSPI/SPI3 | SPI3    | err

// Default pins for SPI(id=1) aka IDF SPI2, can be overridden by a board
#ifndef MICROPY_HW_SPI1_SCK
#ifdef SPI2_IOMUX_PIN_NUM_CLK
// Use IO_MUX pins by default.
// If SPI lines are routed to other pins through GPIO matrix
// routing adds some delay and lower limit applies to SPI clk freq
#define MICROPY_HW_SPI1_SCK SPI2_IOMUX_PIN_NUM_CLK      // pin 14 on ESP32
#define MICROPY_HW_SPI1_MOSI SPI2_IOMUX_PIN_NUM_MOSI    // pin 13 on ESP32
#define MICROPY_HW_SPI1_MISO SPI2_IOMUX_PIN_NUM_MISO    // pin 12 on ESP32
// Only for compatibility with IDF 4.2 and older
#elif CONFIG_IDF_TARGET_ESP32S2
#define MICROPY_HW_SPI1_SCK FSPI_IOMUX_PIN_NUM_CLK
#define MICROPY_HW_SPI1_MOSI FSPI_IOMUX_PIN_NUM_MOSI
#define MICROPY_HW_SPI1_MISO FSPI_IOMUX_PIN_NUM_MISO
#else
#define MICROPY_HW_SPI1_SCK HSPI_IOMUX_PIN_NUM_CLK
#define MICROPY_HW_SPI1_MOSI HSPI_IOMUX_PIN_NUM_MOSI
#define MICROPY_HW_SPI1_MISO HSPI_IOMUX_PIN_NUM_MISO
#endif
#endif

// Default pins for SPI(id=2) aka IDF SPI3, can be overridden by a board
#ifndef MICROPY_HW_SPI2_SCK
#if CONFIG_IDF_TARGET_ESP32
// ESP32 has IO_MUX pins for VSPI/SPI3 lines, use them as defaults
#define MICROPY_HW_SPI2_SCK VSPI_IOMUX_PIN_NUM_CLK      // pin 18
#define MICROPY_HW_SPI2_MOSI VSPI_IOMUX_PIN_NUM_MOSI    // pin 23
#define MICROPY_HW_SPI2_MISO VSPI_IOMUX_PIN_NUM_MISO    // pin 19
#elif CONFIG_IDF_TARGET_ESP32S2 || CONFIG_IDF_TARGET_ESP32S3
// ESP32S2 and S3 uses GPIO matrix for SPI3 pins, no IO_MUX possible
// Set defaults to the pins used by SPI2 in Octal mode
#define MICROPY_HW_SPI2_SCK (36)
#define MICROPY_HW_SPI2_MOSI (35)
#define MICROPY_HW_SPI2_MISO (37)
#endif
#endif

#define MP_HW_SPI_MAX_XFER_BYTES (4092)
#define MP_HW_SPI_MAX_XFER_BITS (MP_HW_SPI_MAX_XFER_BYTES * 8) // Has to be an even multiple of 8

#if CONFIG_IDF_TARGET_ESP32C3
#define HSPI_HOST SPI2_HOST
#elif CONFIG_IDF_TARGET_ESP32S3
#define HSPI_HOST SPI3_HOST
#define FSPI_HOST SPI2_HOST
#endif

typedef struct _machine_hw_spi_default_pins_t {
    int8_t sck;
    int8_t mosi;
    int8_t miso;
} machine_hw_spi_default_pins_t;

typedef struct _extended_spi_transaction_t {
    spi_transaction_t base;
    uint8_t last_used;
    uint8_t queued;
} extended_spi_transaction_t;

typedef struct _transfer_t {
    extended_spi_transaction_t *esp_txn;
    enum { TRANSFER_IDLE, TRANSFER_ACTIVE } state;
} transfer_t;

typedef struct _aspi_obj_t {
    mp_obj_base_t base;
    spi_host_device_t host;
    uint32_t baudrate;
    uint8_t polarity;
    uint8_t phase;
    uint8_t bits;
    uint8_t firstbit;
    int8_t sck;
    int8_t mosi;
    int8_t miso;
    spi_device_handle_t spi;
    enum {
        MACHINE_HW_SPI_STATE_NONE,
        MACHINE_HW_SPI_STATE_INIT,
        MACHINE_HW_SPI_STATE_DEINIT
    } state;
    uint32_t max_transfer_length;
    uint32_t num_ext_txns;
    extended_spi_transaction_t *spi_txns;
    transfer_t *transfer_queue;
    uint8_t transfer_index; // Next transfer item to be used by start_transfer()
    uint8_t max_transfers; // Number of transfers that can be concurrently queued
    mp_obj_t write_buffers;
    mp_obj_t read_buffers;
} aspi_obj_t;

// Default pin mappings for the hardware SPI instances
STATIC const machine_hw_spi_default_pins_t machine_hw_spi_default_pins[2] = {
    { .sck = MICROPY_HW_SPI1_SCK, .mosi = MICROPY_HW_SPI1_MOSI, .miso = MICROPY_HW_SPI1_MISO },
    #ifdef MICROPY_HW_SPI2_SCK
    { .sck = MICROPY_HW_SPI2_SCK, .mosi = MICROPY_HW_SPI2_MOSI, .miso = MICROPY_HW_SPI2_MISO },
    #endif
};

// Static objects mapping to HSPI and VSPI hardware peripherals
STATIC aspi_obj_t machine_hw_spi_obj[2] = {
  {.max_transfer_length = 0,
   .transfer_queue = NULL,
   .transfer_index = 0,
   .max_transfers = 0,
   .num_ext_txns = 0,
  },
  {.max_transfer_length = 0,
   .transfer_queue = NULL,
   .transfer_index = 0,
   .max_transfers = 0,
   .num_ext_txns = 0,
  },
};

const mp_obj_type_t aspi_type;

STATIC void aspi_deinit_internal(aspi_obj_t *self) {
    switch (spi_bus_remove_device(self->spi)) {
        case ESP_ERR_INVALID_ARG:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("invalid configuration"));
            return;

        case ESP_ERR_INVALID_STATE:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("SPI device already freed"));
            return;
    }

    switch (spi_bus_free(self->host)) {
        case ESP_ERR_INVALID_ARG:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("invalid configuration"));
            return;

        case ESP_ERR_INVALID_STATE:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("SPI bus already freed"));
            return;
    }

    int8_t pins[3] = {self->miso, self->mosi, self->sck};

    for (int i = 0; i < 3; i++) {
        if (pins[i] != -1) {
            gpio_pad_select_gpio(pins[i]);
            gpio_matrix_out(pins[i], SIG_GPIO_OUT_IDX, false, false);
            gpio_set_direction(pins[i], GPIO_MODE_INPUT);
        }
    }
}

STATIC void post_txn_cb(spi_transaction_t *txn);

STATIC void aspi_init_internal(
    aspi_obj_t *self,
    int8_t host,
    int32_t baudrate,
    int8_t polarity,
    int8_t phase,
    int8_t bits,
    int8_t firstbit,
    int8_t sck,
    int8_t mosi,
    int8_t miso,
    int32_t max_transfer_length,
    int32_t max_transfers) {
    // if we're not initialized, then we're
    // implicitly 'changed', since this is the init routine
    bool changed = self->state != MACHINE_HW_SPI_STATE_INIT;

    esp_err_t ret;

    aspi_obj_t old_self = *self;

    if (host != -1 && host != self->host) {
        self->host = host;
        changed = true;
    }

    if (baudrate != -1) {
        // calculate the actual clock frequency that the SPI peripheral can produce
        baudrate = spi_get_actual_clock(APB_CLK_FREQ, baudrate, 0);
        if (baudrate != self->baudrate) {
            self->baudrate = baudrate;
            changed = true;
        }
    }

    if (polarity != -1 && polarity != self->polarity) {
        self->polarity = polarity;
        changed = true;
    }

    if (phase != -1 && phase != self->phase) {
        self->phase = phase;
        changed = true;
    }

    if (bits != -1 && bits != self->bits) {
        self->bits = bits;
        changed = true;
    }

    if (firstbit != -1 && firstbit != self->firstbit) {
        self->firstbit = firstbit;
        changed = true;
    }

    if (sck != -2 && sck != self->sck) {
        self->sck = sck;
        changed = true;
    }

    if (mosi != -2 && mosi != self->mosi) {
        self->mosi = mosi;
        changed = true;
    }

    if (miso != -2 && miso != self->miso) {
        self->miso = miso;
        changed = true;
    }

    if (self->host != HSPI_HOST
        #ifdef FSPI_HOST
        && self->host != FSPI_HOST
        #endif
        #ifdef VSPI_HOST
        && self->host != VSPI_HOST
        #endif
        ) {
        mp_raise_msg_varg(&mp_type_ValueError, MP_ERROR_TEXT("SPI(%d) doesn't exist"), self->host);
    }

    // XXX missing: Proper handling of state "changed".
    if (max_transfer_length == -1) {
        max_transfer_length = SPI_MAX_DMA_LEN;
    } else if (max_transfer_length <= 0) {
        mp_raise_msg_varg(
            &mp_type_ValueError,
            MP_ERROR_TEXT("max_transfer_length must be a positive integer "
                          "value, not %d"),
            max_transfer_length);
        return;
    }

    if (max_transfers == -1) {
        max_transfers = 2;
    } else if (max_transfers <= 0 || max_transfers > 255) {
        mp_raise_msg_varg(
            &mp_type_ValueError,
            MP_ERROR_TEXT("max_transfers must be an integer value "
                          "between 1 and 255, not %d"),
            max_transfers);
        return;
    }
    int num_spi_txns =
        (max_transfer_length + SPI_MAX_DMA_LEN - 1) / SPI_MAX_DMA_LEN;
    max_transfer_length = SPI_MAX_DMA_LEN * num_spi_txns;
    if (self->max_transfer_length != max_transfer_length ||
        self->max_transfers != max_transfers) {
        self->max_transfer_length = max_transfer_length;
        self->max_transfers = max_transfers;
        if (self->spi_txns != NULL) {
            free(self->spi_txns);
            self->spi_txns = NULL;
        }
        if (self->transfer_queue != NULL) {
            free(self->transfer_queue);
            self->transfer_queue = NULL;
        }
        self->transfer_queue = malloc(sizeof(transfer_t) * max_transfers);
        if (self->transfer_queue == NULL) {
            mp_raise_msg(
                &mp_type_MemoryError,
                MP_ERROR_TEXT("Cannot allocate memory for SPI transfers"));
            return;
        }
        memset(self->transfer_queue, 0,
               sizeof(transfer_t) * max_transfers);

        self->spi_txns = malloc(
            sizeof(extended_spi_transaction_t)
            * num_spi_txns * max_transfers);
        if (self->spi_txns == NULL) {
            free(self->transfer_queue);
            mp_raise_msg(
                &mp_type_MemoryError,
                MP_ERROR_TEXT("Cannot allocate memory for SPI transfers"));
            return;
        }
        memset(self->spi_txns, 0,
               sizeof(extended_spi_transaction_t)
               * num_spi_txns * max_transfers);
        self->num_ext_txns = num_spi_txns;
        self->transfer_index = 0;
        transfer_t *queue_p = self->transfer_queue;
        extended_spi_transaction_t *spi_txn_p = self->spi_txns;
        for (int queue_index = 0;
             queue_index < max_transfers;
             queue_index++) {
            queue_p->esp_txn = spi_txn_p;
            queue_p += 1;
            spi_txn_p += num_spi_txns;
        }
        if (self->write_buffers != NULL) {
          mp_obj_tuple_del(self->write_buffers);
        }
        // XXX Haven't seen anywhere in the Micropython sources a check if
        // the call of mp_obj_new_tuple() failed. Is this really not necessary?
        self->write_buffers = mp_obj_new_tuple(max_transfers, NULL);
        mp_obj_tuple_t *buf_tuple = self->write_buffers;
        for (int wb_index = 0; wb_index < max_transfers; wb_index++) {
            buf_tuple->items[wb_index] = mp_const_none;
        }
        self->read_buffers = mp_obj_new_tuple(max_transfers, NULL);
        buf_tuple = self->read_buffers;
        for (int wb_index = 0; wb_index < max_transfers; wb_index++) {
            buf_tuple->items[wb_index] = mp_const_none;
        }
    }

    if (changed) {
        if (self->state == MACHINE_HW_SPI_STATE_INIT) {
            self->state = MACHINE_HW_SPI_STATE_DEINIT;
            aspi_deinit_internal(&old_self);
        }
    } else {
        return; // no changes
    }

    spi_bus_config_t buscfg = {
        .miso_io_num = self->miso,
        .mosi_io_num = self->mosi,
        .sclk_io_num = self->sck,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1
    };
    ESP_LOGI("modaspi", "SPI bus config sclk pin %d miso pin %d mosi pin %d quadwp pin %d quadhd pin %d",
             buscfg.sclk_io_num, buscfg.mosi_io_num, buscfg.miso_io_num,
             buscfg.quadwp_io_num, buscfg.quadhd_io_num);

    spi_device_interface_config_t devcfg = {
        .clock_speed_hz = self->baudrate,
        .mode = self->phase | (self->polarity << 1),
        .spics_io_num = -1, // No CS pin
        .queue_size = self->num_ext_txns * self->max_transfers,
        .flags = self->firstbit == MICROPY_PY_MACHINE_SPI_LSB ? SPI_DEVICE_TXBIT_LSBFIRST | SPI_DEVICE_RXBIT_LSBFIRST : 0,
        .pre_cb = NULL,
        .post_cb = post_txn_cb
    };
    ESP_LOGI("modaspi", "SPI device config clock %d mode %d cs pin %d queue size %d flags %d",
             devcfg.clock_speed_hz, devcfg.mode, devcfg.spics_io_num,
             devcfg.queue_size, devcfg.flags);

    // Initialize the SPI bus

    // Select DMA channel based on the hardware SPI host
    int dma_chan = 0;
    #if CONFIG_IDF_TARGET_ESP32S2 || CONFIG_IDF_TARGET_ESP32S3 || CONFIG_IDF_TARGET_ESP32C3
    dma_chan = SPI_DMA_CH_AUTO;
    #else
    if (self->host == HSPI_HOST) {
        dma_chan = 1;
    } else {
        dma_chan = 2;
    }
    #endif

    ret = spi_bus_initialize(self->host, &buscfg, dma_chan);
    switch (ret) {
        case ESP_ERR_INVALID_ARG:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("invalid configuration"));
            return;

        case ESP_ERR_INVALID_STATE:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("SPI host already in use"));
            return;
    }

    ret = spi_bus_add_device(self->host, &devcfg, &self->spi);
    switch (ret) {
        case ESP_ERR_INVALID_ARG:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("invalid configuration"));
            spi_bus_free(self->host);
            return;

        case ESP_ERR_NO_MEM:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("out of memory"));
            spi_bus_free(self->host);
            return;

        case ESP_ERR_NOT_FOUND:
            mp_raise_msg(&mp_type_OSError, MP_ERROR_TEXT("no free slots"));
            spi_bus_free(self->host);
            return;
    }
    self->state = MACHINE_HW_SPI_STATE_INIT;
}

STATIC mp_obj_t aspi_deinit(mp_obj_t self_in) {
    aspi_obj_t *self = (aspi_obj_t *)self_in;
    if (self->state == MACHINE_HW_SPI_STATE_INIT) {
        self->state = MACHINE_HW_SPI_STATE_DEINIT;
        aspi_deinit_internal(self);
    }
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(aspi_deinit_obj, aspi_deinit);

STATIC mp_uint_t gcd(mp_uint_t x, mp_uint_t y) {
    while (x != y) {
        if (x > y) {
            x -= y;
        } else {
            y -= x;
        }
    }
    return x;
}

STATIC int aspi_start_transfer_internal(aspi_obj_t *self, mp_buffer_info_t *src, mp_buffer_info_t *dest) {
    if (src != NULL && dest != NULL && src->len != dest->len) {
        mp_raise_ValueError(MP_ERROR_TEXT(
            "read buffer and write buffer must have the same length."));
    }
    size_t transfer_len = src != NULL ? src->len : dest->len;
    if (transfer_len >= self->max_transfer_length) {
        mp_raise_ValueError(MP_ERROR_TEXT(
            "buffer length must not exceed %d bytes (set during initialisation)"));
    }
    transfer_t *next_transfer = &self->transfer_queue[self->transfer_index];
    /* XXX Missing: an optional timeout parameter, to be oassed to
       spi_device_get_trans_result().
     */
    for (int txn_index = 0; txn_index < self->num_ext_txns; txn_index++) {
        if (next_transfer->esp_txn[txn_index].queued) {
            spi_transaction_t *txn_result;
            spi_device_get_trans_result(self->spi, &txn_result, portMAX_DELAY);
            // This check is a bit paranoid - can be removed once the exception
            // isn't seen for a suffiently long time.
            if (txn_result != (spi_transaction_t*) &next_transfer->esp_txn[txn_index]) {
                mp_raise_msg_varg(
                    &mp_type_RuntimeError,
                    MP_ERROR_TEXT("Unexpected return value from spi_device_get_trans_result(): %p. Should be %p"),
                    txn_result, &next_transfer->esp_txn[txn_index]);
            }
            next_transfer->esp_txn[txn_index].queued = 0;
        }
    }
    next_transfer->state = TRANSFER_ACTIVE;

    uint8_t *wr_ptr = (uint8_t*) (src != NULL ? src->buf : NULL);
    uint8_t *rd_ptr = (uint8_t*) (dest != NULL ? dest->buf : NULL);
    int bytes_to_transfer = src != NULL ? src->len : dest->len;
    int txn_index = 0;
    /* XXX Hrmmm... machine_hw_spi.c calls spi_device_acquire_bus() before
       starting to use the SPI bus. This makes of course sense in order to
       avoid concurrent and thus conflicting access to different SPI
       devices, or from different threads to the same device. Problem for
       this module: The related call of spi_release_bus()
       can't be placed in this C function since the SPI transaction results
       are retrieved in another function call. The best place for a pair of
       spi_device_(acquire|release)_bus() calls would be an (async) context
       manager. But that's a bit tricky to implement in plain C...
       Hence for now just do not use these functions.
    */
    // spi_device_acquire_bus(self->spi, portMAX_DELAY);

    while (bytes_to_transfer > 0) {
      int block_size = bytes_to_transfer > SPI_MAX_DMA_LEN ? SPI_MAX_DMA_LEN : bytes_to_transfer;
      extended_spi_transaction_t *esp_txn = &next_transfer->esp_txn[txn_index];
      memset(esp_txn, 0, sizeof(extended_spi_transaction_t));
      esp_txn->base.length = src != NULL ? 8 * block_size : 0;
      esp_txn->base.rxlength = dest != NULL ? 8 * block_size : 0;
      esp_txn->last_used = block_size <= SPI_MAX_DMA_LEN ? 1 : 0;
      esp_txn->queued = 1;
      esp_txn->base.user = next_transfer;
      esp_txn->base.tx_buffer = wr_ptr;
      esp_txn->base.rx_buffer = rd_ptr;
      spi_device_queue_trans(self->spi, (spi_transaction_t*) esp_txn, portMAX_DELAY);
      if (wr_ptr != NULL) {
          wr_ptr += block_size;
      }
      if (rd_ptr != NULL) {
          rd_ptr += block_size;
      }
      bytes_to_transfer -= block_size;
      txn_index++;
    }

    int result = self->transfer_index;
    self->transfer_index = (self->transfer_index + 1) % self->max_transfers;
    return result;
}

STATIC mp_obj_t aspi_start_transfer(mp_obj_t self_in, mp_obj_t wr_buf, mp_obj_t rd_buf) {
    mp_buffer_info_t src,  dest;
    aspi_obj_t *self = (aspi_obj_t *) self_in;
    if (rd_buf == mp_const_none) {
        if (wr_buf == mp_const_none) {
            mp_raise_ValueError(MP_ERROR_TEXT(
                "At least one of write buffer and read buffer must not be None"));
        }
        mp_get_buffer_raise(wr_buf, &src, MP_BUFFER_READ);
        return MP_OBJ_NEW_SMALL_INT(
            aspi_start_transfer_internal(self, &src, NULL));
    }
    mp_get_buffer_raise(rd_buf, &dest, MP_BUFFER_WRITE);
    if (wr_buf == mp_const_none) {
      return  MP_OBJ_NEW_SMALL_INT(
          aspi_start_transfer_internal(self, NULL, &dest));
    }
    mp_get_buffer_raise(wr_buf, &src, MP_BUFFER_READ);
    return MP_OBJ_NEW_SMALL_INT(
        aspi_start_transfer_internal(self, &src, &dest));
}

STATIC MP_DEFINE_CONST_FUN_OBJ_3(aspi_start_transfer_obj, aspi_start_transfer);

STATIC void post_txn_cb(spi_transaction_t *txn) {
  if (((extended_spi_transaction_t*) txn)->last_used) {
      transfer_t* transfer = (transfer_t*) txn->user;
      transfer->state = TRANSFER_IDLE;
    }
}

STATIC void aspi_finish_transfer_internal(aspi_obj_t *self, int transfer_id) {
    // XXX add timeout parameter?.
    if (transfer_id < 0 || transfer_id >= self->max_transfers) {
        mp_raise_msg_varg(&mp_type_ValueError,
                          MP_ERROR_TEXT("Invalid transfer ID (%d)"),
                          transfer_id);
    }
    transfer_t *transfer = &(self->transfer_queue[transfer_id]);
    // Note:
    // (1) More than one transfer may be in states TRANSFER_ACTIVE or
    //     TRANSFER_READY.
    // (2) This method may be called for a transfer that will be finished later
    //     than another one. This means that the calls of
    //     spi_device_get_trans_result() may return transactions which belong
    //     to another transfer.
    // Hence call spi_device_get_trans_result() and mark the returned
    // transaction as "retrieved" until all transactions of the "wanted"
    // transfer are retrieved.
    extended_spi_transaction_t *wanted_transaction = transfer->esp_txn;
    for (int txn_index = 0; txn_index < self->num_ext_txns; txn_index++) {
        if (wanted_transaction->last_used == 1) {
            break;
        }
        wanted_transaction++;
    }

    spi_transaction_t *queue_result;
    while (wanted_transaction->queued) {
        spi_device_get_trans_result(self->spi, &queue_result, portMAX_DELAY);
            ((extended_spi_transaction_t*) queue_result)->queued = 0;
    }
 }

STATIC mp_obj_t aspi_finish_transfer(mp_obj_t self_in, mp_obj_t transfer_id_in) {
    aspi_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_int_t transfer_id = mp_obj_get_int(transfer_id_in);
    aspi_finish_transfer_internal(self, transfer_id);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_2(aspi_finish_transfer_obj, aspi_finish_transfer);

// write(self, buffer, wait_on_result=True)
STATIC mp_obj_t aspi_write(size_t n_args, const mp_obj_t *args) {
    aspi_obj_t *self = MP_OBJ_TO_PTR(args[0]);
    mp_buffer_info_t src;
    bool wait_on_result = 1;
    mp_get_buffer_raise(args[1], &src, MP_BUFFER_READ);
    if (n_args > 2) {
        wait_on_result = mp_obj_is_true(args[2]);
    }
    int transfer_id = aspi_start_transfer_internal(self, &src, NULL);
    if (wait_on_result) {
      aspi_finish_transfer_internal(self, transfer_id);
    }
    return MP_OBJ_NEW_SMALL_INT(transfer_id);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(aspi_write_obj, 2, 3, aspi_write);

STATIC mp_obj_t aspi_transfer_finished(mp_obj_t self_in, mp_obj_t transfer_id_in) {
    aspi_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_int_t transfer_id = mp_obj_get_int(transfer_id_in);
    if (transfer_id < 0 || transfer_id >= self->max_transfers) {
        mp_raise_msg_varg(&mp_type_ValueError,
                          MP_ERROR_TEXT("Invalid transfer ID (%d)"),
                          transfer_id);
    }
    if (self->transfer_queue[transfer_id].state == TRANSFER_ACTIVE) {
      return mp_const_false;
    }
    return mp_const_true;
 }
STATIC MP_DEFINE_CONST_FUN_OBJ_2(aspi_transfer_finished_obj, aspi_transfer_finished);

// Return: True if a transfer can be started immediately, i.e., if
// the next transfer record to be used is in state TRANSFER_IDLE.
STATIC mp_obj_t aspi_can_start_transfer(mp_obj_t self_in) {
    aspi_obj_t *self = MP_OBJ_TO_PTR(self_in);
    return self->transfer_queue[self->transfer_index].state == TRANSFER_IDLE ?
        mp_const_true : mp_const_false;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(aspi_can_start_transfer_obj, aspi_can_start_transfer);


/******************************************************************************/
// MicroPython bindings for hw_spi

STATIC void machine_hw_spi_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {
    aspi_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_printf(print, "SPI(id=%u, baudrate=%u, polarity=%u, phase=%u, bits=%u, firstbit=%u, sck=%d, mosi=%d, miso=%d, max_transfer_length=%d, max_transfers=%d)",
        self->host, self->baudrate, self->polarity,
        self->phase, self->bits, self->firstbit,
        self->sck, self->mosi, self->miso, self->max_transfer_length,
        self->max_transfers);
}

STATIC mp_obj_t aspi_init(size_t n_args, const mp_obj_t *args, mp_map_t *kw_args) {
    aspi_obj_t *self = (aspi_obj_t *)MP_OBJ_TO_PTR(args[0]);

    enum { ARG_id, ARG_baudrate, ARG_polarity, ARG_phase, ARG_bits,
      ARG_firstbit, ARG_sck, ARG_mosi, ARG_miso, ARG_max_transfer_length,
      ARG_max_transfers };
    static const mp_arg_t allowed_args[] = {
        { MP_QSTR_id,       MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_baudrate, MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_polarity, MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_phase,    MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_bits,     MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_firstbit, MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_sck,      MP_ARG_KW_ONLY | MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_mosi,     MP_ARG_KW_ONLY | MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_miso,     MP_ARG_KW_ONLY | MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_max_transfer_length,
                           MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_max_transfers,
                           MP_ARG_KW_ONLY | MP_ARG_INT, {.u_int = -1} },
    };

    mp_arg_val_t parsed_args[MP_ARRAY_SIZE(allowed_args)];
    mp_arg_parse_all(n_args-1, &args[1], kw_args, MP_ARRAY_SIZE(allowed_args),
        allowed_args, parsed_args);
    int8_t sck, mosi, miso;

    if (parsed_args[ARG_sck].u_obj == MP_OBJ_NULL) {
        sck = -2;
    } else if (parsed_args[ARG_sck].u_obj == mp_const_none) {
        sck = -1;
    } else {
        sck = machine_pin_get_id(parsed_args[ARG_sck].u_obj);
    }

    if (parsed_args[ARG_miso].u_obj == MP_OBJ_NULL) {
        miso = -2;
    } else if (parsed_args[ARG_miso].u_obj == mp_const_none) {
        miso = -1;
    } else {
        miso = machine_pin_get_id(parsed_args[ARG_miso].u_obj);
    }

    if (parsed_args[ARG_mosi].u_obj == MP_OBJ_NULL) {
        mosi = -2;
    } else if (parsed_args[ARG_mosi].u_obj == mp_const_none) {
        mosi = -1;
    } else {
        mosi = machine_pin_get_id(parsed_args[ARG_mosi].u_obj);
    }


    aspi_init_internal(
        self, parsed_args[ARG_id].u_int, parsed_args[ARG_baudrate].u_int,
        parsed_args[ARG_polarity].u_int, parsed_args[ARG_phase].u_int,
        parsed_args[ARG_bits].u_int, parsed_args[ARG_firstbit].u_int, sck,
        mosi, miso, parsed_args[ARG_max_transfer_length].u_int,
        parsed_args[ARG_max_transfers].u_int);
    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_KW(aspi_init_obj, 1, aspi_init);


STATIC mp_obj_t aspi_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *all_args) {
  enum { ARG_id, ARG_baudrate, ARG_polarity, ARG_phase, ARG_bits, ARG_firstbit, ARG_sck, ARG_mosi, ARG_miso, ARG_max_transfer_length, ARG_max_transfers };
    static const mp_arg_t allowed_args[] = {
        { MP_QSTR_id,       MP_ARG_REQUIRED | MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_baudrate, MP_ARG_INT, {.u_int = 500000} },
        { MP_QSTR_polarity, MP_ARG_INT, {.u_int = 0} },
        { MP_QSTR_phase,    MP_ARG_INT, {.u_int = 0} },
        { MP_QSTR_bits,     MP_ARG_INT, {.u_int = 8} },
        { MP_QSTR_firstbit, MP_ARG_INT, {.u_int = MICROPY_PY_MACHINE_SPI_MSB} },
        { MP_QSTR_sck,      MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_mosi,     MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_miso,     MP_ARG_OBJ, {.u_obj = MP_OBJ_NULL} },
        { MP_QSTR_max_transfer_length, MP_ARG_INT, {.u_int = -1} },
        { MP_QSTR_max_transfers, MP_ARG_INT, {.u_int = -1} },
    };
    mp_arg_val_t args[MP_ARRAY_SIZE(allowed_args)];
    mp_arg_parse_all_kw_array(n_args, n_kw, all_args, MP_ARRAY_SIZE(allowed_args), allowed_args, args);

    aspi_obj_t *self;
    const machine_hw_spi_default_pins_t *default_pins;
    if ((args[ARG_id].u_int < 1) || (args[ARG_id].u_int > 2)) {
        mp_raise_msg_varg(
            &mp_type_ValueError,
            MP_ERROR_TEXT("SPI(%d) doesn't exist. Allowed values are 1 and 2"),
            args[ARG_id].u_int);
    }
    if (args[ARG_id].u_int == 1) { // SPI2_HOST which is FSPI_HOST on ESP32Sx, HSPI_HOST on others
        self = &machine_hw_spi_obj[0];
        default_pins = &machine_hw_spi_default_pins[0];
    } else {
        self = &machine_hw_spi_obj[1];
        default_pins = &machine_hw_spi_default_pins[1];
    }
    self->base.type = &aspi_type;

    int8_t sck, mosi, miso;

    if (args[ARG_sck].u_obj == MP_OBJ_NULL) {
        sck = default_pins->sck;
    } else if (args[ARG_sck].u_obj == mp_const_none) {
        sck = -1;
    } else {
        sck = machine_pin_get_id(args[ARG_sck].u_obj);
    }

    if (args[ARG_mosi].u_obj == MP_OBJ_NULL) {
        mosi = default_pins->mosi;
    } else if (args[ARG_mosi].u_obj == mp_const_none) {
        mosi = -1;
    } else {
        mosi = machine_pin_get_id(args[ARG_mosi].u_obj);
    }

    if (args[ARG_miso].u_obj == MP_OBJ_NULL) {
        miso = default_pins->miso;
    } else if (args[ARG_miso].u_obj == mp_const_none) {
        miso = -1;
    } else {
        miso = machine_pin_get_id(args[ARG_miso].u_obj);
    }

    aspi_init_internal(
        self,
        args[ARG_id].u_int,
        args[ARG_baudrate].u_int,
        args[ARG_polarity].u_int,
        args[ARG_phase].u_int,
        args[ARG_bits].u_int,
        args[ARG_firstbit].u_int,
        sck,
        mosi,
        miso,
        args[ARG_max_transfer_length].u_int,
        args[ARG_max_transfers].u_int);

    return MP_OBJ_FROM_PTR(self);
}

/* Left as a comment to remind that the whole approach of this module
 is a bit fragile since it copies many of the internal structures from
 machine_hw_spi.c -- and this can lead to a big mess if machine.SPI and
_ASPI are used to access the same SPI bus...

spi_host_device_t machine_hw_spi_get_host(mp_obj_t in) {
    if (mp_obj_get_type(in) != &machine_spi_type) {
        mp_raise_ValueError(MP_ERROR_TEXT("expecting a SPI object"));
    }
    machine_hw_spi_obj_t *self = (machine_hw_spi_obj_t *)in;
    return self->host;
}
*/

STATIC const mp_rom_map_elem_t aspi_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_init), MP_ROM_PTR(&aspi_init_obj) },
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&aspi_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR_start_transfer), MP_ROM_PTR(&aspi_start_transfer_obj) },
    { MP_ROM_QSTR(MP_QSTR_finish_transfer), MP_ROM_PTR(&aspi_finish_transfer_obj) },
    { MP_ROM_QSTR(MP_QSTR_can_start_transfer), MP_ROM_PTR(&aspi_can_start_transfer_obj) },
    { MP_ROM_QSTR(MP_QSTR_transfer_finished), MP_ROM_PTR(&aspi_transfer_finished_obj) },
    { MP_ROM_QSTR(MP_QSTR_write), MP_ROM_PTR(&aspi_write_obj) },
};

STATIC MP_DEFINE_CONST_DICT(aspi_locals_dict, aspi_locals_dict_table);


MP_DEFINE_CONST_OBJ_TYPE(
    aspi_type,
    MP_QSTR__ASPI,
    MP_TYPE_FLAG_NONE,
    make_new, aspi_make_new,
    print, machine_hw_spi_print,
    locals_dict, &aspi_locals_dict
    );


STATIC const mp_rom_map_elem_t aspi_module_globals_table[] = {
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR__aspi) },
    { MP_ROM_QSTR(MP_QSTR__ASPI), MP_ROM_PTR(&aspi_type) },
};
STATIC MP_DEFINE_CONST_DICT(aspi_module_globals, aspi_module_globals_table);

const mp_obj_module_t aspi_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t *)&aspi_module_globals,
};

MP_REGISTER_MODULE(MP_QSTR__aspi, aspi_module);
